import pandas as pd
import math
import xlsxwriter as xlsw

teamSize = 5

mainFileName = 'student_main.xlsx'
mainData = pd.read_excel(mainFileName)

subFileName = 'student_sub.xlsx'
subData = pd.read_excel(subFileName)

mainDataArr = mainData.to_numpy()
subDataArr = subData.to_numpy()

# 서브 학생수를 최소 몇으로 맞춰야 하는지 구하기
minSubStudent = (teamSize - 1) * len(mainDataArr) # 16 을 맞춰야하는데 현재 서브데이터는 8.
loopCount = math.ceil(minSubStudent / len(subDataArr))

resultSubStudent = []
# 서브 학생수 이어 붙이기 작업 시작
for i in range(loopCount):
    for j in range(len(subDataArr)):
        resultSubStudent.append(subDataArr[j])

resultArr = []
for i in range(len(mainDataArr)):
    resultArr.append(mainDataArr[i])
    # endRange = (i + 1) * (teamSize - 1) - 1
    # startRange = endRange - (teamSize - 2)
    # print(startRange)
    # print(endRange)
    # for j in range(startRange, endRange + 1):
    #     print(j)
    #     print('------------')
    #     resultArr.append(subDataArr[j])

print(resultArr)

resultDf = pd.DataFrame(resultArr)
resultDf = resultDf.transpose()
xlsfile = 'pandas_simple.xlsx'
writer = pd.ExcelWriter(xlsfile, engine='xlsxwriter')

resultDf.to_excel(writer, sheet_name="Sheet1", startrow=1, startcol=1, header=False, index=False)